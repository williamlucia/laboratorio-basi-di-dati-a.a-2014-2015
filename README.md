# Laboratorio di Basi di Dati
## A.A. 2014 - 2015

Materiale relativo al Laboratorio di Basi di Dati per l'anno accademico 2014 - 2015.

Il corso prevede esercitazioni in laboratorio, dove vengono illustrati agli studenti i comandi **SQL** (DDL, DML, DQL) per la creazione di un database, per la creazione/modifica di uno schema relazionale e per l'interrogazione dei dati in un **DBMS** reale.

Durante il corso verrà utilizzato [PostgreSQL](http://www.postgresql.org/) come DBMS.

E' inoltre presente un [gruppo Facebook](https://www.facebook.com/groups/basididati1415/) per interagire con gli studenti. Nel gruppo veranno caricate le slides e inseriti esercizi/temi d'esame su cui gli studenti potranno confrontarsi.