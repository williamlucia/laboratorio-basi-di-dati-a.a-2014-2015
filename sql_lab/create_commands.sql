CREATE TABLE Cliente( codice integer primary key, cognome varchar(50), nome varchar(30), eta int );
CREATE TABLE Filiale( codice varchar(10) primary key, indirizzo varchar(255), citta varchar(100) ); 
CREATE TABLE ContoCorrente( codice integer primary key, filiale varchar(10) references Filiale(codice), cliente integer references Cliente(codice) ON DELETE SET NULL, saldo real ); 
CREATE TABLE Movimenti( cc integer references ContoCorrente(codice), data timestamp, importo real, primary key(cc,data,importo) );