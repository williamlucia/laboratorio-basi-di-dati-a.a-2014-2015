INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (1,'Valencia','Kane',28);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (2,'Horn','Lester',59);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (3,'Douglas','Aiko',47);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (4,'Potter','Yardley',51);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (5,'Livingston','Justin',59);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (6,'Dotson','Noah',52);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (7,'Bruce','Aristotle',20);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (8,'Trevino','Sonia',11);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (9,'Mosley','Florence',50);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (10,'Dickson','Ishmael',61);
INSERT INTO Cliente (codice,cognome,nome,eta) VALUES (11,'Davis','Ira',25);
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('Z0P8N57','8853 Ac Rd.','Saint-Pierre');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('F8Z1B20','215-3723 Interdum. Av.','Völkermarkt');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('V6H3Y40','P.O. Box 811, 4482 Quis Av.','Shimla');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('C9U1R77','P.O. Box 937, 308 Convallis Street','East Gwillimbury');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('Y0L1S50','Ap #980-9840 Et Ave','Lac La Biche County');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('S1O4P79','957-6211 Nunc St.','Giove');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('B2R6N44','P.O. Box 350, 3126 Eu Rd.','Cuddapah');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('V3Y9Z51','P.O. Box 382, 3469 Tincidunt Av.','Sahiwal');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('E3Q1E49','599-5646 Eu, Road','Mansfield-et-Pontefract');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('B6R2V47','5333 Risus. Ave','Penhold');
INSERT INTO Filiale (codice,indirizzo,citta) VALUES ('L7Y3B62','383-7709 Tincidunt St.','Annone di Brianza');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (1,'Z0P8N57',1,'473.25');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (2,'F8Z1B20',2,'114.93');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (3,'V6H3Y40',3,'427.46');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (4,'C9U1R77',4,'868.79');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (5,'Y0L1S50',5,'68.99');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (6,'S1O4P79',6,'158.31');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (7,'B2R6N44',7,'225.59');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (8,'V3Y9Z51',8,'039.46');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (9,'E3Q1E49',9,'792.56');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (10,'B6R2V47',10,'754.61');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (11,'L7Y3B62',1,'917.75');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (12,'F8Z1B20',3,'545.47');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (13,'C9U1R77',5,'790.94');
INSERT INTO ContoCorrente (codice,filiale,cliente,saldo) VALUES (14,'S1O4P79',7,'125.50');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'01/21/2013','381.23');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'06/19/2013','140.00');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'07/25/2013','98.52');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'10/07/2014','405.26');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'03/27/2013','386.29');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'03/31/2013','458.92');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'06/25/2013','308.51');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'11/26/2013','79.47');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'01/12/2014','334.54');
INSERT INTO Movimenti (cc,data,importo) VALUES (10,'11/07/2013','215.28');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'12/06/2014','204.57');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'07/06/2013','246.27');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'03/16/2014','49.24');
INSERT INTO Movimenti (cc,data,importo) VALUES (10,'03/11/2014','394.84');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'04/08/2014','505.78');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'05/23/2014','112.64');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'02/08/2013','519.03');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'04/16/2014','558.88');
INSERT INTO Movimenti (cc,data,importo) VALUES (3,'03/10/2013','83.57');
INSERT INTO Movimenti (cc,data,importo) VALUES (10,'12/15/2014','179.90');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'04/21/2013','494.39');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'09/13/2014','384.23');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'08/04/2014','261.51');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'11/24/2014','301.13');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'12/07/2014','243.33');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'02/04/2014','525.93');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'11/03/2013','82.20');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'05/20/2013','32.10');
INSERT INTO Movimenti (cc,data,importo) VALUES (7,'08/10/2014','408.26');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'04/01/2013','294.40');
INSERT INTO Movimenti (cc,data,importo) VALUES (10,'04/20/2013','456.53');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'11/11/2013','392.46');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'12/15/2014','284.27');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'02/14/2013','228.79');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'07/23/2014','543.98');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'05/30/2013','207.96');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'08/20/2014','187.04');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'05/06/2013','383.39');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'04/16/2014','28.11');
INSERT INTO Movimenti (cc,data,importo) VALUES (3,'12/24/2014','31.25');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'01/22/2013','61.77');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'04/07/2013','116.75');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'05/09/2013','479.48');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'08/03/2014','428.90');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'10/19/2013','429.19');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'05/10/2013','506.48');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'07/30/2014','116.31');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'01/09/2013','562.18');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'07/28/2014','128.43');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'07/13/2014','497.79');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'10/04/2013','378.78');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'08/29/2013','320.05');
INSERT INTO Movimenti (cc,data,importo) VALUES (3,'01/28/2013','220.60');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'08/30/2013','22.25');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'02/13/2014','119.74');
INSERT INTO Movimenti (cc,data,importo) VALUES (3,'06/30/2014','298.59');
INSERT INTO Movimenti (cc,data,importo) VALUES (3,'08/02/2014','104.16');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'11/06/2014','560.95');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'01/11/2014','376.60');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'08/11/2014','441.59');
INSERT INTO Movimenti (cc,data,importo) VALUES (5,'03/09/2013','360.60');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'12/21/2013','101.93');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'02/25/2013','330.04');
INSERT INTO Movimenti (cc,data,importo) VALUES (5,'11/26/2013','294.52');
INSERT INTO Movimenti (cc,data,importo) VALUES (5,'04/01/2014','218.84');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'01/02/2013','151.56');
INSERT INTO Movimenti (cc,data,importo) VALUES (5,'12/06/2014','596.78');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'08/27/2014','519.87');
INSERT INTO Movimenti (cc,data,importo) VALUES (5,'09/06/2014','470.57');
INSERT INTO Movimenti (cc,data,importo) VALUES (7,'05/06/2014','599.28');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'10/22/2013','336.81');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'09/14/2014','60.08');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'01/01/2013','523.63');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'11/13/2013','546.20');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'11/08/2013','255.27');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'05/08/2013','582.77');
INSERT INTO Movimenti (cc,data,importo) VALUES (7,'06/07/2014','576.01');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'08/31/2014','582.40');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'07/10/2013','88.60');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'08/17/2013','128.55');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'02/16/2013','349.99');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'06/21/2013','76.80');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'01/12/2014','547.77');
INSERT INTO Movimenti (cc,data,importo) VALUES (9,'03/19/2014','190.31');
INSERT INTO Movimenti (cc,data,importo) VALUES (8,'01/21/2013','122.06');
INSERT INTO Movimenti (cc,data,importo) VALUES (12,'05/31/2013','491.50');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'12/22/2014','493.01');
INSERT INTO Movimenti (cc,data,importo) VALUES (11,'07/01/2014','300.86');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'02/18/2013','509.42');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'03/17/2014','574.76');
INSERT INTO Movimenti (cc,data,importo) VALUES (1,'07/12/2014','184.25');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'01/09/2013','304.10');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'01/31/2013','163.97');
INSERT INTO Movimenti (cc,data,importo) VALUES (14,'06/30/2014','538.69');
INSERT INTO Movimenti (cc,data,importo) VALUES (7,'06/26/2014','47.70');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'03/23/2013','522.47');
INSERT INTO Movimenti (cc,data,importo) VALUES (2,'12/10/2013','403.53');
INSERT INTO Movimenti (cc,data,importo) VALUES (4,'12/18/2013','532.24');
INSERT INTO Movimenti (cc,data,importo) VALUES (6,'02/17/2013','164.98');
INSERT INTO Movimenti (cc,data,importo) VALUES (13,'10/12/2013','54.90');